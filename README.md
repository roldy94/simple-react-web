# Simple react web

Este proyecto es una aplicacion web hecha con react

## Getting Started

Seguir los pasos de abajo

### Prerequisites

La ultima version estable de node 

```
https://nodejs.org/es/
```

### Installing

Primero clonar el proyecto

```
git clone https://gitlab.com/roldy94/simple-react-web
```

Despues instalar los node modolues

```
npm install
```

Por ultimo instalar json server

```
npm install -g json-server
```

## Running 

Para arrancar la aplicacion (recomiendo primero arrancar el servidor)

```
npm start
```

Para arrancar el server json 

```
cd api
```

```
json-server -p 8080 db.json
```

## Crendeciales para ingresar a la aplicacion

Usuario
```
 admin
```
Contraseña
```
 admin
```

## Versioning

Ninguna

## Authors

* **Nahuel Roldan** - *Initial work* - [Software Developer](https://gitlab.com/roldy94)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* "antd": "^3.21.4",
* "connected-react-router": "^6.5.2",
* "history": "^4.9.0",
* "nprogress": "^0.2.0",
* "react": "^16.9.0",
* "react-dom": "^16.9.0",
* "react-placeholder": "^3.0.2",
* "react-redux": "^7.1.0",
* "react-router-dom": "^5.0.1",
* "react-router-redux": "^4.0.8",
* "react-scripts": "3.0.1",
* "redux-logger": "^3.0.6",
* "redux-saga": "^1.0.5",
* "redux-thunk": "^2.3.0",
* "styled-components": "^4.3.2"
