import { all } from 'redux-saga/effects';
import  loginSaga  from "../features/login/sagas";
import productSaga from "../features/products/sagas";


export default function* rootSaga(getState) {
    yield all([
      loginSaga(),
      productSaga()
    ]);
  }
  