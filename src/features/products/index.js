import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Actions } from "./actions";
import {
    RowProduct,
    LayoutProduct,
    TableProduct,
    ColProduct,
    BadgeProduct,
    ButtonProduct,
    ContainerProducts,
    InputProduct,
    ButtonAddProduct,
    InputProductSearch
} from './styles';
import { message } from 'antd';
import ModalProduct from "./components/modal"

class Products extends React.Component {

    _showModal = () => {
        const { actions } = this.props;
        actions.setVisible(true);
    };

    _showModalUpdate = (request) => {
        const { actions } = this.props;
        actions.setVisible(true);
        actions.setUpdate(true);
        actions.setID(request.id);
        actions.setDescripcion(request.descripcion);
        actions.setPrecio(request.precio);
        actions.setCategoria(request.categoria);
        actions.setCantidad(request.cantidad);
    };

    _handleOk = e => {
        const { actions } = this.props;
        const { descripcion, precio, categoria, cantidad } = this.props.state
        actions.productAdd({
            descripcion: descripcion,
            precio: precio,
            categoria: categoria,
            cantidad: cantidad
        });
    };

    _handleUpdate = e => {
        const { actions } = this.props;
        const { descripcion, precio, categoria, cantidad, id } = this.props.state
        actions.productUpdate({
            id: id,
            descripcion: descripcion,
            precio: precio,
            categoria: categoria,
            cantidad: cantidad
        });
    };

    _handleCancel = e => {
        const { actions } = this.props;
        actions.setVisible(false);
        actions.setUpdate(false);
        actions.clearAll();
    };

    componentDidMount() {
        const { actions } = this.props;
        actions.productRequest();
    }
    _handleDelete = (key) => {
        const { actions } = this.props;
        actions.productDelete(key);
        message.error("Fila Eliminada");
    }

    _handleDelete = (key) => {
        const { actions } = this.props;
        actions.productDelete(key);
        message.error("Fila Eliminada");
    }

    render() {
        const columns = [{
            title: 'Descripción',
            dataIndex: 'descripcion',
            key: 'descripcion',
            width: 400,
            sorter: (a, b) => a.descripcion.length - b.descripcion.length,
        }, {
            title: 'Precio',
            dataIndex: 'precio',
            key: 'precio',
            width: 400,
            sorter: (a, b) => a.precio.length - b.precio.length,
        }, {
            title: 'Categoria',
            dataIndex: 'categoria',
            key: 'categoria',
            width: 400,
        },
        {
            title: 'Inscriptos',
            dataIndex: 'cantidad',
            key: 'cantidad',
            width: 300,
        },
        {
            title: 'Modificar',
            dataIndex: '',
            key: 'x',
            width: 100,
            render: (record) => <ButtonProduct type="primary" shape="circle" icon="edit" onClick={() => this._showModalUpdate(record)} />,
        },
        {
            title: 'Borrar',
            dataIndex: 'id',
            key: 'id',
            width: 100,
            render: (record) => <ButtonProduct type="danger" shape="circle" icon="delete" onClick={() => this._handleDelete(record)} />,
        }];
        const { actions } = this.props;
        return (
            <ContainerProducts>
                <LayoutProduct>
                    <RowProduct style={{ padding: "inherit" }}>

                        <ButtonAddProduct type="primary" onClick={() => this._showModal()}>Agregar</ButtonAddProduct>

                        <ColProduct span={24}>
                            <InputProductSearch onChange={e => actions.setFilter(e.target.value)} placeholder="Filtre por descripcion o categoria"  value={this.props.state.searchValue} />

                            <TableProduct pagination={{ pageSize: 25 }}
                                size="small"
                                columns={columns}
                                bordered={true}
                                title={() => <span>Cursos disponibles: <BadgeProduct count={this.props.state.productsFilter.length} /> </span>}
                                locale={{ emptyText: "No hay productos cargados todavia" }}
                                dataSource={this.props.state.productsFilter} />

                        </ColProduct>
                    </RowProduct>
                </LayoutProduct>
                <ModalProduct btnTextOk={this.props.state.update ? "Modificar" :"Agregar"} visible={this.props.state.visible} title={this.props.state.update ? "Modificar":"Agregar"} handleOk={this.props.state.update ? this._handleUpdate:this._handleOk} handleCancel={this._handleCancel}>

                    <InputProduct onChange={e => actions.setDescripcion(e.target.value)} placeholder="Descripción" value={this.props.state.descripcion} />
                    <InputProduct onChange={e => actions.setPrecio(e.target.value)} placeholder="Precio" value={this.props.state.precio} />
                    <InputProduct onChange={e => actions.setCategoria(e.target.value)} placeholder="Categoria" value={this.props.state.categoria} />
                    <InputProduct onChange={e => actions.setCantidad(e.target.value)} placeholder="Inscriptos" value={this.props.state.cantidad} />

                </ModalProduct>
            </ContainerProducts>
        );
    }
}

function mapStateToProps(state) {
    return {
        state: state.productReducer,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators({ ...Actions }, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Products);