import styled from 'styled-components';
import {
    Badge,
    Col,
    Table,
    Button,
    Layout,
    Row,
    Input
} from 'antd';

export const ContainerProducts = styled.div`
    padding: 24px;
    background: #fff;
    min-height: 500px;
    margin: 16px 0;
`;

export const LayoutProduct = styled(Layout)`
    padding: 24px;
    background: #fff;
    margin: 16px 0; 
`;
export const InputProduct = styled(Input)`
    margin: 5px;
`;

export const TableProduct = styled(Table)``;
export const ColProduct = styled(Col)``;
export const BadgeProduct = styled(Badge)``;
export const ButtonProduct = styled(Button)``;
export const RowProduct = styled(Row)``;

export const ButtonAddProduct = styled(Button)`
    margin-bottom: 10px;
`;

export const InputProductSearch = styled(Input)`
    margin-bottom: 5px;
`;