import React from 'react';
import {
  FormLogin,
  IconLogin,
  InputLogin,
  ButtonLogin,
  LayoutLogin,
  FormItemLogin,
  FooterLogin,
  ContentLogin,
  ContainerLogin,
  ContainerLogo,
  ContainerImg,
} from './styles';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Actions } from './actions';
import { AUTHOR } from '../../constans';

class NormalLoginForm extends React.Component {
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { actions } = this.props;
    return (
      <LayoutLogin>
        <ContentLogin>
          <ContainerLogin>
            <ContainerLogo>
              <ContainerImg src={require('../../assets/logo.png')} />
            </ContainerLogo>
            <FormLogin onSubmit={this.handleSubmit}>
              <FormItemLogin>
                {getFieldDecorator('userName', {
                  rules: [
                    { required: true, message: 'Porfavor ingrese su Usuario!' },
                  ],
                })(
                  <InputLogin
                    prefix={<IconLogin type='user' />}
                    placeholder='Usuario'
                    onChange={(e) => actions.setUsername(e.target.value)}
                  />
                )}
              </FormItemLogin>
              <FormItemLogin>
                {getFieldDecorator('password', {
                  rules: [
                    {
                      required: true,
                      message: 'Porfavor ingrese su Password!',
                    },
                  ],
                })(
                  <InputLogin
                    prefix={<IconLogin type='lock' />}
                    type='password'
                    placeholder='Password'
                    onChange={(e) => actions.setPassword(e.target.value)}
                    onKeyPress={(e) =>
                      e.key === 'Enter'
                        ? actions.loginRequest({
                            username: this.props.state.username,
                            pass: this.props.state.password,
                          })
                        : null
                    }
                  />
                )}
              </FormItemLogin>
              <FormItemLogin>
                <ButtonLogin
                  type='primary'
                  className='login-form-button'
                  onClick={() =>
                    actions.loginRequest({
                      username: this.props.state.username,
                      pass: this.props.state.password,
                    })
                  }
                >
                  Iniciar sesion
                </ButtonLogin>
              </FormItemLogin>
            </FormLogin>
          </ContainerLogin>
        </ContentLogin>
        <FooterLogin>{AUTHOR}</FooterLogin>
        {this.props.state.isLoggedIn
          ? this.props.history.push('/inicio')
          : null}
      </LayoutLogin>
    );
  }
}

const WrappedNormalLoginForm = FormLogin.create()(NormalLoginForm);

function mapStateToProps(state) {
  return {
    state: state.loginReducer,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...Actions }, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WrappedNormalLoginForm);
